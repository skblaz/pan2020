# -*- coding: utf-8 -*-
"""
Created on Sat May 23 20:13:50 2020

@author: Bosec
"""


## parse pan data.
from tqdm import tqdm
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle
from feature_construction import *
import json
import pandas as pd
from functools import reduce
import random
import numpy as np
import os
import logging
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.decomposition import TruncatedSVD
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from xgboost import XGBRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error

import matplotlib.pyplot as plt

def export_labels(labels_file):
    labels = {}
    labels['occupations']=[]
    labels['gender']=[]
    labels['birthyear']=[]
    genders= {'male':0,'female':1}
    occupations = {'sports' : 0, 'performer' : 1 , 'creator' : 2, 'politics':3}
    with open(labels_file) as lf:
        for line in lf:
            lab_di = json.loads(line)
            labels['occupations'].append(occupations[lab_di['occupation']])
            labels['gender'].append(genders[lab_di['gender']])
            labels['birthyear'].append(int(lab_di['birthyear']))
            
    print("Parsed labels..")
    with open(os.path.join("../train_data/","labels.pkl"),mode='wb') as f:
        pickle.dump(labels,f)
    return labels
def export_docs(fname):
    documents = {}
    taken = 0
    with open(os.path.join(fname,"follower-feeds.ndjson")) as fnx:
        for line in tqdm(fnx,total=args.tweets):
            #if random.random() > 0.3:
            lx = json.loads(line)
            lx['text'] = np.array((lx['text']))            
            persons = random.sample(range(len(lx['text'])),min(len(lx['text']),args.persons))
            out = []
            for c in persons:
                idxs = random.sample(range(len(lx['text'][c])), min(args.idxs,len(lx['text'][c])))     
                for i in idxs:
                    out.append(lx['text'][c][i])
                tokens_word = " ".join(out) #lx['text'][0:30])#lx['text'][0:args.num_samples][0:args.num_samples])
                documents[lx['id']] = tokens_word
            taken = taken + 1
            if taken > args.tweets:
                break       
    return documents
def export_big(fname,limit=400):
    documents = {}
    taken = 0        
    idxs = random.sample(range(1920),limit)
    idxs.sort()
    print(idxs)
    with open(os.path.join(fname,"follower-feeds.ndjson")) as fnx:
        for line in tqdm(fnx,total=1920):
           if taken in idxs: 
            lx = json.loads(line)
            lx['text'] = np.array((lx['text'])) 
            print()
            print(len(lx['text']))
            persons = random.sample(range(len(lx['text'])),min(len(lx['text']),5))
            out = []
            for x in lx['text'][persons]:
                out = out + x
            tokens_word = " ".join(out) #lx['text'][0:30])#lx['text'][0:args.num_samples][0:args.num_samples])
            documents[lx['id']] = tokens_word
           taken = taken + 1            
    print("Parsed docs..")
    with open(os.path.join("../train_data/","documents.pkl"),mode='wb') as f:
        pickle.dump(documents,f)
    return documents,idxs
import seaborn as sns
def train(dataframe,final_y,output=False):    
    report = []
    trained_models = {}    
    for nrep in range(2):
        for nfeat in [1500,3000,5000,10000,15000]:
            for dim in [256,512,768]:
                tokenizer, feature_names, data_matrix = get_features(dataframe, max_num_feat = nfeat, labels = final_y)
                reducer = TruncatedSVD(n_components = min(dim, nfeat * len(feature_names)-1))
                data_matrix = reducer.fit_transform(data_matrix)
                X_train, X_test, y_train, y_test = train_test_split(data_matrix, final_y, train_size=0.8, test_size=0.2)
                logging.info("Generated {} features.".format(nfeat*len(feature_names)))
                parameters = {"loss":["hinge","log"],"penalty":["elasticnet"],"alpha":[0.01,0.001,0.0001,0.0005],"l1_ratio":[0.05,0.25,0.3,0.6,0.8,0.95],"power_t":[0.5,0.1,0.9]}
                svc = SGDClassifier()
                clf1 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = 8,cv = 10, refit = True)
                clf1.fit(X_train, y_train)
                logging.info(str(max(clf1.cv_results_['mean_test_score'])) +" training configuration with best score (SVM)")
                predictions = clf1.predict(X_test)
                acc_svm = accuracy_score(predictions,y_test)
                logging.info("Test accuracy score SVM {}".format(acc_svm))
                parameters = {"C":[0.1,1,10,25,50,100,500],"penalty":["l2"]}
                svc = LogisticRegression(max_iter = 100000)
                clf2 = GridSearchCV(svc, parameters, verbose = 0, n_jobs = 8,cv = 10, refit = True)
                clf2.fit(X_train, y_train)
                logging.info(str(max(clf1.cv_results_['mean_test_score'])) +" training configuration with best score (LR)")
                predictions = clf2.predict(X_test)
                acc_lr = accuracy_score(predictions,y_test)
                logging.info("Test accuracy score LR {}".format(acc_lr))
                trained_models[nfeat] = ((clf1, clf2), dim)
                report.append([nfeat, acc_lr, acc_svm])              
    
    dfx = pd.DataFrame(report)
    dfx.columns = ["Number of features","LR","SVM"]
    dfx = pd.melt(dfx, id_vars=['Number of features'], value_vars=['LR','SVM'])
    sns.lineplot(dfx['Number of features'],dfx['value'], hue = dfx["variable"], markers = True, style = dfx['variable'])
    plt.legend()
    plt.tight_layout()
    plt.savefig("../tfidf-1.png",dpi = 300)
    sorted_dfx = dfx.sort_values(by = ["value"])
    print(sorted_dfx.iloc[-1,:])
    max_acc = sorted_dfx.iloc[-1,:][['Number of features','variable']]

    final_feature_number = max_acc['Number of features']
    final_learner = max_acc['variable']
    logging.info(" Final feature number: {}, final learner: {}".format(final_feature_number, final_learner))
    
    if final_learner == "SVM":
        index = 0        
    else:
        index = 1

    clf_final, dim = trained_models[final_feature_number]
    clf_final = clf_final[index]
    tokenizer, feature_names, data_matrix = get_features(dataframe, max_num_feat = final_feature_number)
    reducer = TruncatedSVD(n_components = min(dim, nfeat * len(feature_names)-1)).fit(data_matrix)
    return tokenizer, clf_final, reducer

def find_age(dataframe,final_y,output=False):    
    print(final_y)
    report = []
    trained_models = {}    
    for nfeat in [12000,15000,17000]:
        for dim in [256,512,768]:
            tokenizer, feature_names, data_matrix = get_features(dataframe, max_num_feat = nfeat, labels = final_y)
            reducer = TruncatedSVD(n_components = min(dim, nfeat * len(feature_names)-1))
            data_matrix = reducer.fit_transform(data_matrix)
            X_train, X_test, y_train, y_test = train_test_split(data_matrix, final_y, train_size=0.8, test_size=0.2)
            logging.info("Generated {} features.".format(nfeat*len(feature_names)))
            parameters = {'nthread':[4], #when use hyperthread, xgboost may become slower
              'objective':['reg:linear'],
              'learning_rate': [0.001, 0.01, 0.1,], 
              'max_depth': [5, 6, 7],
              'min_child_weight': [4],
              'silent': [1],
              'subsample': [0.3,0.7],
              'colsample_bytree': [0.7],
              'n_estimators': [100,200,500]}            
            xgb = XGBRegressor() 
            clf2 = GridSearchCV(xgb, parameters, cv = 3,
                        n_jobs = -1,
                        verbose=True)
            clf2.fit(X_train, y_train)
            acc_xgb = mean_squared_error(y_test, clf2.predict(X_test))              
            logging.info(str(sum(clf2.cv_results_['mean_test_score'])/len(clf2.cv_results_['mean_test_score'])) +" training configuration with best score (LR)")
            logging.info("Test accuracy MSE score LR {}".format(acc_xgb))
            parameters = {'kernel': ('linear', 'rbf','poly'), 
                          'C':[1.5,10],
                          'gamma': [1e-7, 1e-4],
                          'epsilon':[0.1,0.2,0.5,0.3],
                          }
            svr = svm.SVR()
            clf1 = GridSearchCV(svr, parameters, cv = 3,
                        n_jobs = -1,
                        verbose=True)
            clf1.fit(X_train, y_train)
            logging.info(str(sum(clf1.cv_results_['mean_test_score'])/len(clf1.cv_results_['mean_test_score'])) +" training configuration with best score (SVM)")
            predictions = clf1.predict(X_test)
            acc_svm =  mean_squared_error(predictions,y_test)
            logging.info("Test accuracy MSE score SVM regression {}".format(acc_svm))

            trained_models[nfeat] = ((clf1, clf2), dim)
            report.append([nfeat, acc_xgb, acc_svm])              
    
    dfx = pd.DataFrame(report)
    dfx.columns = ["Number of features","SVM","XGB"]
    dfx = pd.melt(dfx, id_vars=['Number of features'], value_vars=['SVM','XGB'])
    sns.lineplot(dfx['Number of features'],dfx['value'], hue = dfx["variable"], markers = True, style = dfx['variable'])
    plt.legend()
    plt.tight_layout()
    plt.savefig("../tfidf-3.png",dpi = 300)
    sorted_dfx = dfx.sort_values(by = ["value"])
    print(sorted_dfx.iloc[0,:])
    max_acc = sorted_dfx.iloc[0,:][['Number of features','variable']]
    final_feature_number = max_acc['Number of features']
    final_learner = max_acc['variable']
    logging.info(" Final feature number: {}, final learner: {}".format(final_feature_number, final_learner))    
    if final_learner == "SVM":
        index = 0        
    else:
        index = 1
    clf_final, dim = trained_models[final_feature_number]
    clf_final = clf_final[index]
    tokenizer, feature_names, data_matrix = get_features(dataframe, max_num_feat = final_feature_number)
    reducer = TruncatedSVD(n_components = min(dim, nfeat * len(feature_names)-1)).fit(data_matrix)
    return tokenizer, clf_final, reducer

def search(fname):
    labels = pickle.load(open(os.path.join("../train_data/","labels.pkl"),'rb'))
    #documents,idxs = export_big(fname,150)#pickle.load(open(os.path.join("../train_data/","documents.pkl"),'rb'))
    dataframe = pickle.load(open(os.path.join("../train_data/","dataframe_f.pkl"),'rb')) 
    #build_dataframe(documents)
    print("Dataframe BUILT")
    #with open(os.path.join("../train_data/","dataframe.pkl"),mode='wb') as f:
    #    pickle.dump(dataframe,f)
    #GENDER
    labels['birthyear']=np.array((labels['birthyear']))
    tokenizer, clf_final, reducer = find_age(dataframe,labels['birthyear'],False)
    _export(tokenizer,clf_final,reducer,'birthyear')
    #OCCUPATION
    labels['occupations']=np.array((labels['occupations']))
    tokenizer, clf_final, reducer = train(dataframe,labels['occupations'],False)
    _export(tokenizer,clf_final,reducer,'occupation')
    #GENDER
    labels['gender']=np.array((labels['gender']))
    tokenizer, clf_final, reducer = train(dataframe,labels['gender'],False)
    _export(tokenizer,clf_final,reducer,'gender')
    
    
def _export(tokenizer,clf,reducer,id_="f"):
    with open(os.path.join("../train_data/","tokenizer_"+id_+".pkl"),mode='wb') as f:
        pickle.dump(tokenizer,f)
    with open(os.path.join("../train_data/","clf_"+id_+".pkl"),mode='wb') as f:
        pickle.dump(clf,f)
    with open(os.path.join("../train_data/","reducer_"+id_+".pkl"),mode='wb') as f:        
        pickle.dump(reducer,f)

def parse_feeds(fname, labels_file, all=False):
    documents = export_big(fname,150)
    ## pan features
    print("Building Dataframe")
    dataframe = build_dataframe(documents)
    print('Dataframe built')
    with open(os.path.join("../train_data/","dataframe_f.pkl"),mode='wb') as f:
        pickle.dump(dataframe,f)
          
def _import(path_in="../train_data/"):
    """Imports tokenizer,clf,reducer from param(path_in, default is ../models)"""
    tokenizer = pickle.load(open(os.path.join(path_in,"tokenizer_f.pkl"),'rb'))
    reducer = pickle.load(open(os.path.join(path_in,"reducer_f.pkl"),'rb'))
    data_matrix = pickle.load(open(os.path.join(path_in,"data_matrix_f.pkl"),'rb'))
    labels = pickle.load(open(os.path.join(path_in,"labels.pkl"),'rb'))
    return tokenizer,reducer,data_matrix,labels

def fit_import(path_in="../train_data/"):
    """Imports tokenizer,clf,reducer from param(path_in, default is ../models)"""
    tokenizer = pickle.load(open(os.path.join(path_in,"tokenizer_f.pkl"),'rb'))
    clf = pickle.load(open(os.path.join(path_in,"clf-f.pkl"),'rb'))
    reducer = pickle.load(open(os.path.join(path_in,"reducer_f.pkl"),'rb'))
    return tokenizer,clf,reducer
        
def tic():
    import time
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()

def toc():
    import time
    if 'startTime_for_tictoc' in globals():
        print("Elapsed time is " + str(time.time() - startTime_for_tictoc) + " seconds.")
    else:
        print("Toc: start time not set")
        
def _import_fit(path_in="../train_data/",id_=""):
    """Imports tokenizer,clf,reducer from param(path_in, default is ../models)"""
    tokenizer = pickle.load(open(os.path.join(path_in,"tokenizer_"+id_+".pkl"),'rb'))
    clf = pickle.load(open(os.path.join(path_in,"clf_"+id_+".pkl"),'rb'))
    reducer = pickle.load(open(os.path.join(path_in,"reducer_"+id_+".pkl"),'rb'))
    return tokenizer,clf,reducer
    
def fit(path,out_path="../out"):
    tic()
    test_texts = {}
    inv_g = {0:'male',1:'female'}
    inv_o = {0:'sports',1:'performer',2:'creator',3:'politics'}
    """
    clfs = {}
    tknz = {}
    reds = {}
    id_ = 'occupation'
    tknz[id_],clfs[id_],reds[id_] = _import_fit('../train_data/',id_)    
    id_ = 'birthyear'
    tknz[id_],clfs[id_],reds[id_] = _import_fit('../train_data/',id_)    
    id_ = 'gender'
    tknz[id_],clfs[id_],reds[id_] = _import_fit('../train_data/',id_)  
    """
    with open(os.path.join(path,"follower-feeds.ndjson")) as fnx:
        for line in tqdm(fnx):
                lx = json.loads(line)
                lx['text'] = np.array((lx['text']))            
                persons = random.sample(range(len(lx['text'])),min(len(lx['text']),args.persons))
                out = []
                for c in persons:
                    idxs = random.sample(range(len(lx['text'][c])), min(args.idxs,len(lx['text'][c])))     
                    for i in idxs:
                        out.append(lx['text'][c][i])
                tokens_word = " ".join(out) #lx['text'][0:30])#lx['text'][0:args.num_samples][0:args.num_samples])
                test_texts[lx['id']] = tokens_word                
    f = open(os.path.join(out_path,"labels.ndjson"),mode='w')    
    df_text = build_dataframe(test_texts)
    idxs = list(test_texts.keys())
    test_texts = {}
    del test_texts
    #GENDER
    pred = {}
    for id_ in ['occupation','gender','birthyear']:
        tkn,lfs,reds = _import_fit('./ff_diffs/train_data/',id_)          
        matrix_form = tkn.transform(df_text)
        reduced_matrix = reds.transform(matrix_form)
        pred[id_] = lfs.predict(reduced_matrix)
    occupation = pred['occupation']
    gender = pred['gender']
    birthyear = pred['birthyear']
    cnt = 0
    for x in idxs:                 
        o = inv_o[int(occupation[cnt])]
        g = inv_g[int(gender[cnt])]
        y = min(1999,max(1940,int(birthyear[cnt])))
        item = {"id": x, "occupation": o, "gender":g, "birthyear": y}
        print(item)
        v = json.dumps(item)
        cnt = cnt + 1
        f.write(v + '\n')  
    toc()
    f.close()
        
if __name__ == "__main__":
    from scipy import io
    import argparse
    data_inpt = "../../data/pan20-celebrity-profiling-training-dataset-2020-02-28"
    labels_inpt = "../../data/pan20-celebrity-profiling-training-dataset-2020-02-28/labels.ndjson"
    datafolder = "../train_data"    
    
    argparser = argparse.ArgumentParser(description='Author Profiling Evaluation')
    argparser.add_argument('-o', '--output', dest='output', type=str, default='../out',
                           help='Choose output directory')
    argparser.add_argument('-i', '--input', dest='input', type=str,
                           default=data_inpt,
                           help='Choose input dataset')
    argparser.add_argument('-p', '--persons', dest='persons', type=int,
                           default=20,
                           help='Choose dataset size dataset')
    argparser.add_argument('-idx', '--idxs', dest='idxs', type=int,
                           default=4,
                           help='Choose dataset size dataset')
    argparser.add_argument('-t', '--tweets', dest='tweets', type=int,
                           default=1920,
                           help='Choose dataset size dataset') 
    args = argparser.parse_args()
    path = args.input
    path_out = args.output
    #export_labels(labels_inpt)
    #search(data_inpt)
    fit(path,path_out)
