# Organisation
### ./src - contains source code
### ./models - contains model data that is exported from model from ./src
### ./out - local directory for testing
### ./expr - contains experiments data
### ./data - contains data has subfolders en / es for each language respectfully
# Evaluation
## TO RUN LOCALLY 
python3 evaluate.py -i ../data -o ../out

## TO RUN ON TIRA
./run.sh $inputDataset $outputDataset

# Models
## D2V embedding
* source_file :  [d2vModel](./src/D2Vmodel.py)
* D2V_EPOCHS = 50
* D2V_LR = 0.01
* D2V_VS = 100 
### Random forest preformance
![D2V random forest representation](./imgs/rf-d2v.png)
### SVM preformance
![D2V support vector machine](./imgs/svm-d2v.png)
## TFIDF base embedding
* source_file :  [TfIdfModel](./src/TfIdfModel.py)
### Random forest preformance
![TFIDF random forest representation](./imgs/rf-tfidf.png)
### SVM preformance
![TFIDF support vector machine](./imgs/svm-tfidf.png)

### TFIDF extended with truncated SVD
## Properties 
* source_file :  [SvdTFIDF](./src/extended_tfidf.py)
* TFIDF Grid search made on 2500,5000,10000,15000 features
* SVD dimensions reduced serach on 256,512,768
* Tested models LR on SGD, SVM  
 
![SVD on TFIDF support vector machine](./imgs/tfidif-expanded-2.png)


### TFIDF extended with truncated SVD
## Properties 
* source_file :  [SvdTFIDFMerged](./src/tfidf_tira.py)
* 5000 features
* SVD dimensions reduced serach on 256,512,768
* Model :  LR  
 
![SVD on Merged data TFIDF support vector machine](./imgs/tfidif-expanded.png)


### TPOT model search
## Properties
* source_file :  [TPOT](./src/tpot_main.py)
* 120 minutes of search yielded LR regression with F1 score of 0.76

### TFIDF MERGED with truncated SVD 5fCV
## Properties 
* source_file :  [SvdTFIDFMerged](./src/tfidf_tira.py)
* 2500 features
* MERGED english and spanish
* SVD dimensions reduced serach on 256,512,768
* Model :  LR  
* Local score : 0.75
  
![SVD on Merged data TFIDF support vector machine 5fCV](./imgs/tfidif-expanded-5fCV.png)

### TFIDF extended with truncated SVD 5fCV
## Properties 
* source_file :  [SvdTFIDFMerged](./src/tfidf_tira.py)
* 2500 features
* SVD dimensions reduced serach on 256,512,768
* Model :  LR  

  
![SVD on Merged data TFIDF support vector machine 5fCV](./imgs/tfidif-merged-cv-expanded.png)


## Final run 11.05.2020
### Models trained on Train/Test 0.9 Split with 5fCV
| Vectorization | log | Model name | English F1  | Spanish F1  | 
| ------------- |:---------:|:-------------:| -----:|-----:|
| MERGED TDIDF with SVD 2-LANG| [train.log](./models/LR-MERG-CV/train.log) | SVD-LR-SEP-CV |  0.9100 | 0.9367 | 
| MERGED TDIDF with SVD 1- LANG | [train.log](./models/LR-MERG-CV/train.log) | SVD-LR-MERG-CV |  0.9600 | 0.7667 | 
| MERGED TDIDF with SVD NO-HASH | [train.log](./models/LR-MERG-CV/train.log) | SVD-SVM-MERG-CV |  0.9300 | 0.9067 | 


## Final run 05.05.2020
| Vectorization | Model name | English F1  | Spanish F1  | 
| ------------- |:-------------:| -----:|-----:|
| MERGED TDIDF with SVD | SVD-LR-MERG | 0.9633 | 0.9867 | 
| MERGED TDIDF with SVD TweetTokenizer | SVD-LR-BIG-TWT | 0.9633 | 0.9533 | 
| TDIDF with SVD | SVD-SGD-SVM | 0.97  | 0.49  |
| TDIDF with SVD | SVD-LR-1M  |  0.93 | 0.96 |
| TDIDF with SVD BIGREG | SVD-LR-1M  | 0.9367 | 0.9533 |
| TDIDF with SVD | SVD-LR-1BATCH |0.5567  | 0.7033 | 

## Results on TIRA evaluation:
| Vectorization | Model name | English F1  | Spanish F1  | 
| ------------- |:-------------:| -----:|-----:|
| TDIDF with SVD | SVD-SGD-SVM | 0.97  | 0.49  |
| TDIDF with SVD | SVD-LR-1M  |  0.93 | 0.96 |
| MERGED TDIDF with SVD | SVD-LR-MERG | 0.9633 | 0.9867 | 
| MERGED TDIDF with SVD TweetTokenizer | SVD-LR-MERG-2 | 0.9633 | 0.9533 | 

## Results localy: 
| Vectorization | Model name | English F1  | Spanish F1  | 
| ------------- |:-------------:| -----:|-----:|
| TDIDF with SVD | SVD-LR-1M  | 0.97  | x  |
| TDIDF with SVD | SVD-SGD-SVM|  0.93 | 0.96 |
| TFIDF base | TFIDF-SVM-RBFC - 9 | 0.64 | x |
| TFIDF base | TFIDF-SVM-POLY-5 | 0.66 | x |
| TFIDF base | TFIDF-SVM-linear-9 | 0.63 | x |
| TFIDF base | TFIDF-RF-4 | 0.66 | x |
| TFIDF base | TFIDF-LR-NOREG | 0.64 | x |
| D2V base | TFIDF-SVM-RBFC - 9 | 0.64 | x |
| D2V base | TFIDF-SVM-POLY-1 | 0.61 | x |
| D2V base | TFIDF-SVM-linear-8 | 0.64 | x |
| D2V base | TFIDF-RF-10 | 0.62 | x |
| D2V base | TFIDF-LR-NOREG | 0.58 | x |
| TPOT | LR-2H | 0.75 | x |
| TPOT | LR-1H | 0.75 | x |