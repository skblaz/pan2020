import os
import csv
from gensim import utils
import gensim.parsing.preprocessing as gsp
from gensim.models.doc2vec import TaggedDocument, Doc2Vec
from sklearn.base import BaseEstimator
from sklearn import utils as skl_utils
from tqdm import tqdm
from gensim.models.doc2vec import TaggedDocument, Doc2Vec
from sklearn.base import BaseEstimator
from sklearn import utils as skl_utils
from tqdm import tqdm
import multiprocessing
import numpy as np
import pickle
from sklearn.datasets import load_linnerud
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
import parse_data
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
import time
from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
import config 
import matplotlib
import matplotlib.pyplot as plt

MODEL_NAME = "doc2Vec"

def readListDir(path):
    "Returns list of articles present in the FS."
    return os.listdir(path)

def preprocess(s):
    filters = [
           gsp.strip_tags, 
           gsp.strip_punctuation,
           gsp.strip_multiple_whitespaces,
           gsp.strip_numeric,
           gsp.remove_stopwords, 
           gsp.strip_short, 
           gsp.stem_text
    ]
    s = s.lower()
    s = utils.to_unicode(s)
    for f in filters:
        s = f(s)
    return s

class Doc2VecTransformer(BaseEstimator):
    def __init__(self,docs, vector_size=config.D2V_VS, learning_rate=config.D2V_LR, epochs=config.D2V_EPOCHS):
        self.docs = docs
        self.learning_rate = learning_rate
        self.epochs = epochs
        self._model = None
        self.vector_size = vector_size
        self.workers = multiprocessing.cpu_count() - 1
    def fit(self):
        tagged_x = []
        for doc in self.docs:
            temp_doc = TaggedDocument(self.docs[doc].split(),[doc])
            tagged_x.append(temp_doc)            
        model = Doc2Vec(documents=tagged_x, vector_size=self.vector_size, workers=self.workers)
        for epoch in range(self.epochs):
            model.train(skl_utils.shuffle([x for x in tqdm(tagged_x)]), total_examples=len(tagged_x), epochs=1)
            model.alpha -= self.learning_rate
            model.min_alpha = model.alpha
        self._model = model
        return self

    def transform(self):
       return np.asmatrix(np.array([self._model.infer_vector(docs[doc].split()) for doc in self.docs]))
    
    def doc2feature(self,doc):
       return np.array(self._model.infer_vector(self.docs[doc].split()))

    def _export(self,path="..\models\d2vmodel.pkl"):
        self._model.save(path)

    def _import(self,path="..\models\d2vmodel.pkl"):
        self._model = pickle.load(open(path,'rb'))

#TO DO:
#Change path addressing 
#Add it to make features also for TC task
#Cast doc2feature only once to speed up
def buildXY(doc2vec,yRaw):
    """Builds features and target variables"""
    Y = np.empty((0,1), int)
    X = np.empty((0,config.D2V_VS), int)
    for id_ in yRaw:
        x = doc2vec.doc2feature(id_)
        y = yRaw[id_]
        X = np.vstack((X, np.array(x))) 
        Y = np.vstack((Y, y)) 
    return X,Y

def train(X,Y,output=False):
    X_train, X_test, y_train, y_test = train_test_split(X, Y, train_size=0.7, test_size=0.3)    
    if(output):
        fname = f"{config.PATH_EXPR}/{MODEL_NAME}{str(int(time.time()))}.csv"
        f = open(fname, mode='w',newline='')
        f = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    svm_fig, ax = plt.subplots()
    for kernel in ('linear', 'poly', 'rbf'):
        out_c = list(range(1,10))
        pred_c = []
        for c in out_c:
            clf = svm.SVC(kernel=kernel, C=c).fit(X_train, y_train.ravel())
            p = clf.predict(X_test)
            f1 = f1_score(p,y_test,average='macro')
            pred_c.append(f1)
            if(output):
                f.writerow(["SVM","KER:\t"+kernel+"\tC=\t"+str(c),f1])
        ax.plot(out_c, pred_c,label = kernel)            
    ax.legend()            
    ax.set(xlabel='C', ylabel='F1 score', title='D2V SVM')    
    svm_fig.savefig(os.path.join(config.PATH_IMGS,"svm-d2v.png"))
    if(output):
        plt.show()        
    #RF
    rf_fig, ax = plt.subplots()
    depth_c = list(range(2,30))
    pred_c = []
    for depth in depth_c:        
        clf = RandomForestClassifier(max_depth=depth).fit(X_train,y_train.ravel())
        p = clf.predict(X_test)
        f1 = f1_score(p,y_test,average='macro')
        pred_c.append(f1)
        if(output):
            f.writerow(["RF","DEPTH:"+str(depth),f1])
    ax.plot(depth_c, pred_c)    #ax.legend()            
    ax.set(xlabel='DEPTH OF RF', ylabel='F1 score', title='D2V RANDOM FOREST')    
    rf_fig.savefig(os.path.join(config.PATH_IMGS,"rf-d2v.png"))    
    if(output):
        plt.show()        
    #L1  
    clf = LogisticRegression().fit(X_train, y_train.ravel())
    p = clf.predict(X_test)
    if(output):
            f.writerow(["LREG","",f1_score(p,y_test.ravel(),average='macro')])
                     
if __name__ == '__main__':
    XRaw,YRaw = parse_data.export()
    doc2vec = Doc2VecTransformer(XRaw).fit()
    X,Y = buildXY(doc2vec,YRaw)
    train(X,Y,True) 
    print(":) Done.")