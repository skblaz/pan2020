# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 14:10:21 2020

@author: Bosec
"""
import numpy
import os
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
import parse_data
from tpot import TPOTClassifier
from sklearn.model_selection import train_test_split
from feature_construction import *
from sklearn.decomposition import TruncatedSVD
import pickle
import config

X,Y = parse_data.export()
final_y = []
final_texts = []
for k,v in Y.items():
    text = X[k]
    label = v
    final_texts.append(text)
    final_y.append(v)
final_y = np.array(final_y, dtype=np.float32)
#dataframe = build_dataframe(final_texts)
#tokenizer, feature_names, data_matrix = get_features(dataframe, max_num_feat = 20000)
#reducer = TruncatedSVD(n_components = min(1024,1000 * len(feature_names)-1))
#data_matrix = reducer.fit_transform(data_matrix)
vectorizer = TfidfVectorizer(ngram_range=(1, 3),max_features=20000)
X = vectorizer.fit_transform(final_texts).todense()
X_train, X_test, y_train, y_test = train_test_split(X, final_y, train_size=0.8, test_size=0.2)
tpot = TPOTClassifier(max_time_mins=1,n_jobs=-1)
tpot.fit(X_train, y_train)
scores = tpot.score(X_test, y_test)
print('Scores:', scores)   
tpot.export("tpot_C.py")
print('Scores:', scores)   
cnt = len([name for name in os.listdir('./tpot') if os.path.isfile(name)])
tpot.export("tpot/tpot_C_"+str(cnt)+".py")
