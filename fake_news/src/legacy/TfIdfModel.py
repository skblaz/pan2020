import xml.etree.ElementTree as ET
import config 
import numpy
import os
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
import parse_data
import time
import csv
import config 
import matplotlib
import matplotlib.pyplot as plt

MODEL_NAME = "tfidf"
def tfidf(X,Y):    
    outs = []
    for x in X:
        outs.append([X[x],int(Y[x])])
    a = numpy.array(outs)
    X = a[:,0]    
    X = TfidfVectorizer().fit_transform(X)
    y = a[:,1]
    return X,y

def train(X,Y,output=False):
    X_train, X_test, y_train, y_test = train_test_split(X, Y, train_size=0.7, test_size=0.3)    
    if(output):
        fname = f"{config.PATH_EXPR}/{MODEL_NAME}{str(int(time.time()))}.csv"
        f = open(fname, mode='w',newline='')
        f = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    svm_fig, ax = plt.subplots()
    for kernel in ('linear', 'poly', 'rbf'):
        out_c = list(range(1,10))
        pred_c = []
        for c in out_c:
            clf = svm.SVC(kernel=kernel, C=c).fit(X_train, y_train.ravel())
            p = clf.predict(X_test)
            f1 = f1_score(p,y_test,average='macro')
            pred_c.append(f1)
            if(output):
                f.writerow(["SVM","KER:\t"+kernel+"\tC=\t"+str(c),f1])
        ax.plot(out_c, pred_c,label = kernel)            
    ax.legend()            
    ax.set(xlabel='C', ylabel='F1 score', title='TF-IDF SVM C vs F1')    
    svm_fig.savefig(os.path.join(config.PATH_IMGS,"svm-tfidf.png"))
    if(output):
        plt.show()
        
    #RF
    rf_fig, ax = plt.subplots()
    depth_c = list(range(2,30))
    pred_c = []
    for depth in depth_c:        
        clf = RandomForestClassifier(max_depth=depth).fit(X_train,y_train.ravel())
        p = clf.predict(X_test)
        f1 = f1_score(p,y_test,average='macro')
        pred_c.append(f1)
        if(output):
            f.writerow(["RF","DEPTH:"+str(depth),f1])
    ax.plot(depth_c, pred_c)    #ax.legend()            
    ax.set(xlabel='DEPTH OF RF', ylabel='F1 score', title='TF-IDF RANDOM FOREST')    
    rf_fig.savefig(os.path.join(config.PATH_IMGS,"rf-tfidf.png"))    
    if(output):
        plt.show()        
    #L1  
    clf = LogisticRegression().fit(X_train, y_train.ravel())
    p = clf.predict(X_test)
    if(output):
            f.writerow(["LREG","",f1_score(p,y_test.ravel(),average='macro')])

def export():
    X = main()
    Y = readLabels()
    return X,Y
       
if __name__ == "__main__":
    XRaw,YRaw = parse_data.export()
    X,Y = tfidf(XRaw,YRaw)
    train(X,Y,True) 
    print(":) Done.")