import pandas as pd

#.
train_df = pd.read_csv('train.csv', header=None)
train_df.head()

eval_df = pd.read_csv('test.csv', header=None)
eval_df.head()

print(train_df[0])
print("TUKA")
#train_df[0] = (train_df[0] == 2).astype(int)
#eval_df[0] = (eval_df[0] == 2).astype(int)

train_df = pd.DataFrame({
    'text': train_df[0].replace(r'\n', ' ', regex=True),
    'label':train_df[1]
})

print(train_df.head())

eval_df = pd.DataFrame({
    'text': eval_df[0].replace(r'\n', ' ', regex=True),
    'label':eval_df[1]
})

print(eval_df.head())


from simpletransformers.classification import ClassificationModel


# Create a TransformerModel
model = ClassificationModel('roberta', 'roberta-base')

# Train the model
model.train_model(train_df)

# Evaluate the model
result, model_outputs, wrong_predictions = model.eval_model(eval_df)